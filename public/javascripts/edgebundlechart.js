class EdgeBundleChart {
  constructor (container = "") {
    this.rootData = undefined
    if (container) {
      this.container = d3.select(container)
      this.clear()
      const bcr = this.container.node().getBoundingClientRect()
      this.width = bcr.width
    }
    else {
      this.container = d3.select((container) ? container : undefined)
      this.width = 700
    }
    this.svg = undefined
    this.leavesGroupData = undefined
    this.height = this.width
    this.radius = Math.min(this.width, this.height)/2
    this.arcWeight = this.radius/30
    this.arc = d3.arc().cornerRadius(2)
    this.opacityDefault = 0.8
    this.colorout = "#f00"
    this.colorin = "#00f"
    this.colornone = "#ccc"
    this.colorScale = undefined
    this.groupNames = undefined
    this.nodePosition = new Map
    this.arcPosition = new Map
    this.line = d3.lineRadial()
                  .curve(d3.curveBundle.beta(0.85))
                  .radius(d => d.y)
                  .angle(d => d.x)
    this.charId = (new Date()).getTime()
  }
  build (rootData, leavesGroupData, newContainer) {
    if (newContainer) {
      this.container = d3.select(newContainer)
      this.container.selectAll("*").remove()
      const bcr = this.container.node().getBoundingClientRect()
      this.setSize(bcr.width, bcr.width)
    }
    // Armazena os dados
    this.leavesGroupData = leavesGroupData
    this.rootData = this.root(rootData)

    // Cria SVG
    this.svg = this.container.append("svg")
      .attr("id", `eb-${this.charId}`)
      .attr("width", this.width)
      .attr("height", this.height)
      .attr("viewBox", [-this.width / 2, -this.height / 2, this.width, this.height])
    
    // Nos do grafico
    this.svg.append("g")
        .attr("class", "nodes")
        .attr("font-family", "sans-serif")
        .attr("font-size", 10)
      .selectAll("g")
      .data(this.root().leaves(), d => d.data.leaf)
      .enter()
      .append("g")
        .attr("class", "node-group")
        .attr("transform", d => `rotate(${d.x * 180 / Math.PI - 90}) translate(${d.y+this.arcWeight+6},0)`)
        .each(d => this.nodePosition.set(d.data.leaf, d.x))
      .append("text")
        .attr("opacity", d => d.outgoing.length || d.incoming.length ? 1 : 0.4 )
        .attr("fill", d=> d3.color(this.getColorScale(d.data.group)).darker())
        .attr("dy", "0.31em")
        .attr("text-anchor", d => d.x < Math.PI ? "start" : "end")
        .attr("transform", d => d.x >= Math.PI ? "rotate(180)" : null)
        .text(d => d.data.leaf)
        .each(function(d) { d.text = this; })        
        .on("mouseover", (e, d) => {
          this.overed(d)
        })
        .on("mouseout", (e, d) => {
          this.outed(d)
        })
        .append("title")
        .text(d => `${d.data.leaf} - ${d.data.group}\nAlunos saem para ${d.outgoing.length} municípios\nRecebe alunos de ${d.incoming.length} municípios`)

    const groups = this.getGroupData(this.root().leaves())

    // Grupos
    const group = this.svg.append("g")
        .attr("class", "groups")
      .selectAll("g")
      .data(groups, d => d.group)
      .enter()
      .each(d => this.arcPosition.set(d.group, {
        startAngle: d.startAngle,
        endAngle: d.endAngle,
        innerRadius: d.innerRadius,
        outerRadius: d.outerRadius,
      }))
      .append("g")
      .attr("class", "group")
      .on("mouseover", (e, d) => {
        this.overedArc(d,`eb-${this.charId}`)
      })
      .on("mouseout", (e, d) => {
        this.outedArc(d,`eb-${this.charId}`)
      })

    // Arco do grupo
    group.append("path")
      .attr("class", "group-arc")
      .attr("fill", d => this.getColorScale(d.group))
      .attr("opacity", this.opacityDefault)
      .attr("d", this.arc)
      .attr("id", (d, i) => `eb-${this.charId}-group${d.index}`)
      .each(function(d) { d.arc = this; })        
      .append("title").text(d => `${d.group}`)

    // Titulo do arco
    group.append("text")
    .attr("class", "group-text")
      .attr("font-family", "sans-serif")
      .attr("font-size", this.arcWeight-4)
      .attr("fill", d => d3.color(this.getColorScale(d.group)).darker(4))
    .attr("dy", `${this.arcWeight/1.8}pt`)
    .attr("dx", `${this.arcWeight/4}pt`)
    .append("textPath")
    .each(d => (d.charId = this.charId))
    .attr("xlink:href",function(d,i){return `#eb-${d.charId}-group${d.index}`;})
    .text(d => d.group)
    .attr("id", (d, i) => `eb-${this.charId}-textGroup${d.index}`)
    .append("title").text(d => `${d.group}`)

    // Linhas do grafico
    const link = this.svg.append("g")
        .attr("class", "group-line")
        .attr("stroke", this.colornone)
        .attr("fill", "none")
      .selectAll("path")
      .data(this.root().leaves().flatMap(leaf => leaf.outgoing), d => {
        return `${d[0].data.leaf.replace(/\s+/g,'')}_${d[1].data.leaf.replace(/\s+/g,'')}`
      })
      .enter()
      .append("path")
        .attr("class", "line")
        .style("mix-blend-mode", "multiply")
        .attr("d", ([i, o]) => this.line(i.path(o)))
        .each(function(d) { d.path = this; })

    // Eventos
  }

  getGroupData(rootLeaves) {
    return rootLeaves.reduce((resultArr, d, _, rl) => {
      let groupIdx = resultArr.findIndex(e => e[0] === d.data.group)
      if (groupIdx < 0){
        resultArr.push([d.data.group, [d.x], d.y, [d], rl.length])
      }
      else {
        resultArr[groupIdx][1].push(d.x)
        resultArr[groupIdx][2] = Math.max(resultArr[groupIdx][2], d.y)
        resultArr[groupIdx][3].push(d)
      }
      return resultArr
    }, []).map((d,i,arr) => {
      const medAngle = d3.mean(d3.extent(d[1]))
      const arcAngle = (2*Math.PI/(d[4]+arr.length))*(d[1].length)
      return {
        index: i,
        group: d[0],
        startAngle: medAngle - (arcAngle/2),
        endAngle: medAngle + (arcAngle/2),
        innerRadius: d[2] + 3,
        outerRadius: d[2] + this.arcWeight + 3,
        leafs: d[3]
      }
    })
  }

  overedArc(d, svg) {
    d3.select(`#${svg}`)
      .selectAll(".group-arc")
      .filter(dd => dd.index !== d.index)
      .attr("opacity", "0.1")

    d.leafs.forEach(leaf => {
      this.overed(leaf)
    })
  }

  outedArc(d, svg) {
    d3.select(`#${svg}`)
      .selectAll(".group-arc")
      .filter(dd => dd.index !== d.index)
      .attr("opacity", this.opacityDefault)

    d.leafs.forEach(leaf => {
      this.outed(leaf)
    })
  }

  overed(d) {
    d3.select(d.text).attr("font-weight", "bold")
    d3.selectAll(d.incoming.map(d => d.path)).attr("stroke", this.colorin).raise();
    d3.selectAll(d.incoming.map(([d]) => d.text)).attr("fill", this.colorin).attr("font-weight", "bold");
    d3.selectAll(d.outgoing.map(d => d.path)).attr("stroke", this.colorout).raise();
    d3.selectAll(d.outgoing.map(([, d]) => d.text)).attr("fill", this.colorout).attr("font-weight", "bold");
  }

  outed(d) {
    d3.select(d.text).attr("font-weight", null);
    d3.selectAll(d.incoming.map(d => d.path)).attr("stroke", null);
    d3.selectAll(d.incoming.map(([d]) => d.text)).attr("fill", d => d3.color(this.getColorScale(d.data.group)).darker(4)).attr("font-weight", null);
    d3.selectAll(d.outgoing.map(d => d.path)).attr("stroke", null);
    d3.selectAll(d.outgoing.map(([, d]) => d.text)).attr("fill", d => d3.color(this.getColorScale(d.data.group)).darker(4)).attr("font-weight", null);
  }
  
  root(newData) {
    if (!this.rootData || newData) {
      this.rootData = this.tree(this.bilink(this.hierarchy(newData)))
    }
    return this.rootData
  }

  tree(bilinkData){
    return d3.cluster().size([2 * Math.PI, this.radius - 100])(bilinkData)
  }

  bilink(hierarchyData) {
    const map = new Map(hierarchyData.leaves().map(d => [d.data.leaf, d]));
    for (const d of hierarchyData.leaves()) d.incoming = [], d.outgoing = d.data.links.map(i => [d, map.get(i)]);
    for (const d of hierarchyData.leaves()) for (const o of d.outgoing) o[1].incoming.push(o);
    
    return hierarchyData;
  }

  hierarchy(newData) {
      const nodes = new Map(this.leavesGroupData
        .map(c => [c.leaf, {
          leaf: c.leaf, 
          group: c.group, 
          links: []
        }]))

      newData.forEach(d => {
        let node = nodes.get(d.leaf);
        if(d.leaf !== d.link && node && nodes.has(d.link)) {
          if(!node.links.includes(d.link)){
            node.links.push(d.link)
          }
        }
      })
      const nodeGroup = new Map
      const groupNames = this.getGroupNames()

      nodes.forEach(node => {
        if (groupNames.includes(node.group)) {
          let group = nodeGroup.get(node.group);
          if (group === undefined) {
            nodeGroup.set(node.group, group = {group: node.group, children: []})
          }
          group.children.push(node)
        }
      })
      const hierarchyData =  {name: 'hierarchy', children: [...nodeGroup.values()]}
      return d3.hierarchy(hierarchyData).sort(
        (a, b) => d3.ascending(a.height, b.height) || d3.ascending(a.data.id, b.data.id)
      )
  }

  getColorScale(groupName) {
    if (this.colorScale === undefined) {
      const groupNames = this.getGroupNames()
      // const seqColorScale = d3.scaleSequential(d3.interpolateTurbo).domain([0,groupNames.length])
      // const colors = Array(groupNames.length).fill(0).map((_,i) => seqColorScale(i))
      const colors = d3.schemeTableau10 

      this.colorScale = d3.scaleOrdinal()
                          .domain(groupNames)
                          .range(colors)
    }
    return this.colorScale(groupName)
  }

  getGroupNames() {
    if (this.groupNames === undefined) {
      this.groupNames = Array.from(new Set(this.leavesGroupData.map(d => d.group)))
    }
    return this.groupNames
  }

  clear() {
    if (!this.container.empty()){
      this.container.html("")
    }
  }

  setSize(w, h){
    this.width = w
    this.height = h
    this.radius = Math.min(this.width, this.height)/2
    this.arcWeight = this.radius/30
  }

  update (newData, leavesGroupData) {
    if (leavesGroupData) {
      this.colorScale = undefined
      this.groupNames = undefined
      this.leavesGroupData = leavesGroupData
    }
    this.root(newData)
    let groups = this.getGroupData(this.root().leaves())
    const rootLeaves = this.root().leaves()

    // Seleções
    const nodeGroupSelection = this.svg
      .selectAll(".node-group")
      .data(rootLeaves, d => d.data.leaf)
    const groupSelection =  this.svg
      .select(".groups")
      .selectAll(".group")
      .data(groups, d => d.group)

    // Remove eventos
    this.svg.selectAll("*")
      .on("mouseover", null)
      .on("mouseout", null)
    
    // Remover elementos sem dados
    nodeGroupSelection.exit()
      .transition()
      .duration(500)
      .attrTween("opacity", () => d3.interpolate(1, 0))
      .remove()

    groupSelection
      .exit()
      .transition()
      .duration(500)
      .attrTween("opacity", () => d3.interpolate(1, 0))
      .remove()

    this.svg
      .select(".group-line")
      .selectAll(".line")
      .data(this.root().leaves().flatMap(leaf => leaf.outgoing), d => {
        return `${d[0].data.leaf.replace(/\s+/g,'')}_${d[1].data.leaf.replace(/\s+/g,'')}`
      })
      .exit()
      .transition()
      .duration(500)
      .attrTween("opacity", () => d3.interpolate(1, 0))
      .remove()

    // Atualizar elementos
    nodeGroupSelection
      .each(function(d) { d.text = this; }) 
      .on("mouseover", (e, d) => {
        this.overed(d)
      })
      .on("mouseout", (e, d) => {
        this.outed(d)
      })
      .call(g => {
        g.transition()
        .duration(1000)
        .attrTween("transform", d => this.textTweent(d, this))
        .on("end", function (d) { 
            d3.select(this)
            .select("text")
            .attr("opacity", d => d.outgoing.length || d.incoming.length ? 1 : 0.4 )
            .attr("text-anchor", d.x <= Math.PI ? "start" : "end")
            .attr("transform", d.x > Math.PI ? "rotate(180)" : null)
            .select("title")
            .text(d => `${d.data.leaf} - ${d.data.group}\nAlunos saem para ${d.outgoing.length} municípios\nRecebe alunos de ${d.incoming.length} municípios`)
        })
      })
      .select("text")
        .transition()
        .duration(500)
        .attr("opacity", d => d.outgoing.length || d.incoming.length ? 1 : 0.4 )
        .select("title")
        .text(d => `${d.data.leaf} - ${d.data.group}\nAlunos saem para ${d.outgoing.length} municípios\nRecebe alunos de ${d.incoming.length} municípios`)

    groupSelection
      .on("mouseover", (e, d) => {
        this.overedArc(d,`eb-${this.charId}`)
      })
      .on("mouseout", (e, d) => {
        this.outedArc(d,`eb-${this.charId}`)
      })
      .call(d => {
        d.select("text")
          .select("textPath")
          .each(dd => (dd.charId = this.charId))
          .attr("xlink:href",function(dd,i){return `#eb-${dd.charId}-group${dd.index}`;})
          .attr("id", (dd, i) => `eb-${this.charId}-textGroup${dd.index}`)
      })
      .select(".group-arc")
      .transition()
      .duration(500)
      .attrTween("d", a => this.arcTween(a, this))
    
    this.svg
      .select(".group-line")
      .selectAll(".line")
      .data(this.root().leaves().flatMap(leaf => leaf.outgoing), d => {
        return `${d[0].data.leaf.replace(/\s+/g,'')}_${d[1].data.leaf.replace(/\s+/g,'')}`
      })
      .transition()
      .duration(500)
      .attr("d", ([i, o]) => this.line(i.path(o)))
      .each(function(d) { d.path = this; })

    // Adicionar novos elementos
    this.svg
      .selectAll(".nodes")
      .selectAll("g")
      .data(rootLeaves, d => d.data.leaf)
      .enter()
      .append("g")
        .attr("class", "node-group")
        .attr("transform", d => `rotate(${d.x * 180 / Math.PI - 90}) translate(${d.y+this.arcWeight+6},0)`)
        .each(d => this.nodePosition.set(d.data.leaf, d.x))
      .append("text")
        .attr("fill", d=> d3.color(this.getColorScale(d.data.group)).darker())
        .attr("dy", "0.31em")
        .attr("text-anchor", d => d.x < Math.PI ? "start" : "end")
        .attr("transform", d => d.x >= Math.PI ? "rotate(180)" : null)
        .text(d => d.data.leaf)
        .each(function(d) { d.text = this; }) 
        .on("mouseover", (e, d) => {
          this.overed(d)
        })
        .on("mouseout", (e, d) => {
          this.outed(d)
        })
        .transition()
        .duration(500)
        .attrTween("opacity", (d) => d3.interpolate(0.1, (d.outgoing.length  || d.incoming.length ? 1 : 0.4)))

    const group = this.svg
      .select(".groups")
      .selectAll("g")
      .data(groups, d => d.group)
      .enter()
      .each(d => this.arcPosition.set(d.group, {
        startAngle: d.startAngle,
        endAngle: d.endAngle,
        innerRadius: d.innerRadius,
        outerRadius: d.outerRadius,
      }))
      .append("g")
      .attr("class", "group")
      .on("mouseover", (e, d) => {
        this.overedArc(d,`eb-${this.charId}`)
      })
      .on("mouseout", (e, d) => {
        this.outedArc(d,`eb-${this.charId}`)
      })
      .call(g => {
        g.transition()
        .duration(500)
        .attrTween("opacity", () => d3.interpolate(0, 1))
      })
      

    group.append("path")
      .attr("class", "group-arc")
      .attr("fill", d => this.getColorScale(d.group))
      .attr("opacity", this.opacityDefault)
      .attr("d", this.arc)
      .attr("id", (d, i) => `eb-${this.charId}-group${d.index}`)
      .each(function(d) { d.arc = this; })        
      .append("title")
      .text(d => `${d.group}`)

    group.append("text")
      .attr("class", "group-text")
      .attr("font-family", "sans-serif")
      .attr("font-size", this.arcWeight-4)
      .attr("fill", d => d3.color(this.getColorScale(d.group)).darker(4))
      .attr("dy", `${this.arcWeight/1.8}pt`)
      .attr("dx", `${this.arcWeight/4}pt`)
      .append("textPath")
      .each(d => (d.charId = this.charId))
      .attr("xlink:href",function(d,i){return `#eb-${d.charId}-group${d.index}`;})
      .text(d => d.group)
      .attr("id", (d, i) => `eb-${this.charId}-textGroup${d.index}`)

    // Linhas do grafico
    this.svg.select(".group-line")
      .selectAll("path")
      .data(rootLeaves.flatMap(leaf => leaf.outgoing), d => {
        return `${d[0].data.leaf.replace(/\s+/g,'')}_${d[1].data.leaf.replace(/\s+/g,'')}`
      })
      .enter()
      .append("path")
        .attr("class", "line")
        .style("mix-blend-mode", "multiply")
        .attr("d", ([i, o]) => this.line(i.path(o)))
        .each(function(d) { d.path = this; })
        .transition()
        .duration(500)
        .attrTween("opacity", () => d3.interpolate(0, 1))
  }
  
  textTweent(e, chart) {
    const oldAngle = chart.nodePosition.get(e.data.leaf)*180/Math.PI-90
    const newAngle = e.x*180/Math.PI-90
    chart.nodePosition.set(e.data.leaf, e.x)

    const i = d3.interpolate(oldAngle,newAngle)
    return function(t) {
      return `rotate(${i(t)}) translate(${e.y+chart.arcWeight+6},0)`
    }
  }
  
  arcTween(a, chart) {
    const oldArcProp = this.arcPosition.get(a.group)

    this.arcPosition.set(a.group, {
      startAngle: a.startAngle,
      endAngle: a.endAngle,
      innerRadius: a.innerRadius,
      outerRadius: a.outerRadius,
    })

    const i = d3.interpolate(oldArcProp, a);
    this._current = i(0);
    return function(t) {
      return chart.arc(i(t));
    }
  }
}