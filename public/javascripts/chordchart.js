class ChordChart {
  constructor (container = "") {
    this.data = undefined
    if (container) {
      this.container = d3.select(container)
      this.clear()
      const bcr = this.container.node().getBoundingClientRect()
      this.width = bcr.width
    }
    else {
      this.container = d3.select((container) ? container : undefined)
      this.width = 700
    }
    this.height = this.width
    this.innerRadius = Math.min(this.width, this.height) * 0.5 - 90
    this.outerRadius = this.innerRadius + 8
    this.opacityDefault = 0.8
    this.svg = undefined
    this.chords = undefined
    this.groupNames = undefined
    this.ribbons = undefined
    this.groupPath = undefined
    this.colorScale = undefined
    this.ribbonPosition = new Map
    this.arcPosition = new Map
    this.textPosition = new Map
    this.tooltip = d3.select(".tooltip")
      .style("width","text.length + 'px'")
    this.ribbon = d3.ribbonArrow()
      .radius(this.innerRadius - 0.5)
      .padAngle(1 / this.innerRadius)
    this.arc = d3.arc()
      .innerRadius(this.innerRadius)
      .outerRadius(this.outerRadius)
    this.recommendedStage = new Map([
        [this.ageCategory(0), "Creche"],
        [this.ageCategory(1), "Creche"],
        [this.ageCategory(2), "Creche"],
        [this.ageCategory(3), "Creche"],
        [this.ageCategory(4), "Pré-escola"],
        [this.ageCategory(5), "Pré-escola"],
        [this.ageCategory(6), "Fundamental"],
        [this.ageCategory(7), "Fundamental"],
        [this.ageCategory(8), "Fundamental"],
        [this.ageCategory(9), "Fundamental"],
        [this.ageCategory(10), "Fundamental"],
        [this.ageCategory(11), "Fundamental"],
        [this.ageCategory(12), "Fundamental"],
        [this.ageCategory(13), "Fundamental"],
        [this.ageCategory(14), "Fundamental"],
        [this.ageCategory(15), "Médio"],
        [this.ageCategory(16), "Médio"],
        [this.ageCategory(17), "Médio"]
      ])
  }
  
  build (data, newContainer) {
    if (newContainer) {
      this.container = d3.select(newContainer)
      this.clear()
      const bcr = this.container.node().getBoundingClientRect()
      this.setSize(bcr.width, bcr.width)
    }
    // Armazena os dados
    this.data = data

    // Iniciar SVG
    this.svg = this.container.append("svg")
      .attr("width", this.width)
      .attr("height", this.height)
      .attr("viewBox", [-this.width / 2, -this.height / 2, this.width, this.height])
      .attr("font-size", 12)
      .attr("font-family", "sans-serif")
    
    // Grupos
    const group = this.svg.append("g")
      .attr("class", "group-group")
      .selectAll("g")
      .data(this.generateChords().groups)
      .enter()
      .each(d => (d.visibility = true))
      .append("g")
      .attr("class", "group")
      .on("mouseover", (e, d) => {
        if(d.visibility === true) {
          // Mostra tooltip
          this.tooltipControl(true, e, this.numberSeparatorFormat(d.value))
          // Ativa highlight
          this.highlightGroup(true, d)
        }
      })
    .on("mouseout", (e, d) => {
        if(d.visibility === true) {
          // Oculta tooltip
          this.tooltipControl(false)
          // Desativa highlight
          this.highlightGroup(false, d)
        }
      })

    // Arco do grupo
    this.groupPath = group.append("path")
      .attr("class", "group-path")
      .attr("name", c => c.name)
      .attr("fill", d => this.getColorScale(this.getGroupNames(d.index)))
      .each(d => this.arcPosition.set(d.name, {
        startAngle: d.startAngle,
        endAngle: d.endAngle,
      }))
      .attr("d", this.arc)
      .each(function(d) { this._current = d; })
      .attr("id", (d, i) => "group" + d.index) //add id 

    // Texto do grupo
    const groupText = group.append("text")
      .attr("class", "group-text")
      .each(d => {
        d.angle = (d.startAngle + d.endAngle) / 2
        this.textPosition.set(this.getGroupNames(d.index), d.angle)
      })
      .attr("dy", "0.35em")
      .attr("transform", d => `rotate(${(d.angle * 180 / Math.PI - 90)})
        translate(${this.outerRadius + 5})
        ${d.angle > Math.PI ? "rotate(180)" : ""}
      `)
      .attr("text-anchor", d => d.angle > Math.PI ? "end" : null)
      .text(d => this.getGroupNames(d.index))

    // Cordas
    this.ribbons = this.svg.append("g")
        .attr("class", "ribbons-group")
        .attr("fill-opacity", this.opacityDefault)
        .attr("stroke-opacity", this.opacityDefault)
      .selectAll("path")
      .data(this.generateChords(), c => c.fromTo)
      .enter()
        .append("path")
        .each(d => (d.visibility = true))
        .each(d => this.ribbonPosition.set(d.fromTo, {
          source: {startAngle: d.source.startAngle, endAngle: d.source.endAngle, radius: d.source.radius},
          target: {startAngle: d.target.startAngle, endAngle: d.target.endAngle, radius: d.target.radius}
        }))
        .attr("class", "ribbons")
        .attr("fromTo", c => c.fromTo)
        .attr("d", this.ribbon)
        .each(function(d) { this._current = d; })
        .attr("fill", d => this.getColorScale(this.getGroupNames(d.target.index)))
        .attr("stroke", d => d3.rgb(this.getColorScale(this.getGroupNames(d.target.index))).darker())
        .attr("stroke-width", "0.3px")
      
    this.ribbons.on("mouseover", (e, d) => {
        if(d.visibility === true) {
          // Mostra tooltip
          this.tooltipControl (true, e, this.numberSeparatorFormat(d.source.value))
          // Ativa highlight
          this.highlightRibbon(true, d)
        }
      })
      
    this.ribbons.on("mouseout", (e,d) => {
        if(d.visibility === true) {
          // Mostra tooltip
          this.tooltipControl (false)
          // Desativa highlight
          this.highlightRibbon(false, d)
        }
      })
  }

  getColorScale (groupName) {
    if (this.colorScale === undefined) {
      const years = ["00 anos", "01 anos", "02 anos", "03 anos", "04 anos", "05 anos", "06 anos", "07 anos", "08 anos", "09 anos", "10 anos", "11 anos", "12 anos", "13 anos", "14 anos", "15 anos", "16 anos", "17 anos", "18+ anos"]
      const stages = ["EJA", "Técnico", "Médio", "Fundamental",  "Pré-escola", "Creche" ]
      
      const yearColorScale = d3.scaleSequential(d3.interpolateGreys).domain([1,years.length])
      const yearColors = new Array(years.length).fill(0).map((_,i) => yearColorScale(i))

      const stageColorScale = d3.scaleOrdinal(d3.schemeAccent).domain([1, stages.length])
      const stageColors = new Array(stages.length).fill(0).map((_,i) => stageColorScale(i))
      
      this.colorScale = d3.scaleOrdinal([...years, ...stages],[...yearColors, ...stageColors])
    }
    return this.colorScale(groupName)
  }

  getGroupNames(index) {
    if (this.groupNames === undefined) {
      // Armazena o nome dos grupos
      const names = Array.from(new Set(this.data.flatMap(d => {
        if (d.NU_IDADE >= 0 && d.TP_ETAPA_ENSINO.length > 0){
          return [this.ageCategory(d.NU_IDADE), d.TP_ETAPA_ENSINO]
        }
      })))
      
      // Faz a ordenação dos nomes dos grupos
      const sortedNames = ["00 anos", "01 anos", "02 anos", "03 anos", "04 anos", "05 anos", "06 anos", "07 anos", "08 anos", "09 anos", "10 anos", "11 anos", "12 anos", "13 anos", "14 anos", "15 anos", "16 anos", "17 anos", "18+ anos", "EJA", "Técnico", "Médio", "Fundamental",  "Pré-escola", "Creche" ].filter(e => names.includes(e))  
      this.groupNames = sortedNames
    }
    return (index >= 0) ? this.groupNames[index] : this.groupNames
  }

  ageCategory(age) {
    return (age >= 18 ? "18+" : (age < 10 ? "0" + age : age)) + " anos"
  }

  generateChords(newData) {
    if (!this.chords || newData) {
      // Nomes de grupos
      const groupNames = this.getGroupNames()

      // Cria a matriz
      const index = new Map(groupNames.map((name, i) => [name, i]));
      const matrix = Array.from(index, () => new Array(groupNames.length).fill(0));

      const cData = newData ? newData : this.data

      for (const {NU_IDADE, TP_ETAPA_ENSINO} of cData){
        if (NU_IDADE >= 0 && TP_ETAPA_ENSINO.length > 0){
          matrix[index.get(this.ageCategory(NU_IDADE))][index.get(TP_ETAPA_ENSINO)] += 1
        }
      }
      
      // Cria cordas
      const chord = d3.chordDirected()
        .padAngle(10/this.innerRadius)
        .sortSubgroups(d3.descending)
        .sortChords(d3.descending)
      
      this.chords = chord(matrix)

      this.chords.groups.map(g => (g.name = groupNames[g.index]))
      this.chords.map(chord => {
        const source = chord.source
        const target = chord.target
        chord.fromTo = `${groupNames[source.index].replace(/\s+/g,'')}_${groupNames[target.index].replace(/\s+/g,'')}`
          chord.visibility = true
        }
      )
    }
    return this.chords
  }

  tooltipControl (enable, event, txt) {
    this.tooltip.transition()
          .duration(enable ? 200 : 500)
          .style("opacity", enable ? this.opacityDefault : 0)
    if (enable) {
      this.tooltip.html("Total de Alunos: " + txt)
          .style("left", (event.pageX + 5) + "px")
          .style("top", (event.pageY - 28) + "px");
    }
  }

  highlightGroup(enable, d) {
    // Realce das cordas
    this.svg
      .selectAll(".ribbons")
      .filter(dd => dd.source.index != d.index && dd.target.index != d.index && dd.visibility === true)
      .transition()
      .style("fill-opacity", enable ? 0.1 : this.opacityDefault)
      .style("stroke-opacity", enable ? 0.1 : this.opacityDefault)
    // Realce dos grupos
    this.svg
      .selectAll(".group-path")
      .filter(dd => dd.index !== d.index && dd.visibility === true)
      .transition()
      .style("fill-opacity", enable ? 0.1 : this.opacityDefault)
  }

  highlightRibbon(enable, d) {
    // Realce das cordas
    this.svg
      .selectAll(".ribbons")
      .filter(dd => dd !== d && dd.visibility === true)
      .transition()
      .style("fill-opacity", enable ? 0.1 : this.opacityDefault)
      .style("stroke-opacity", enable ? 0.1 : this.opacityDefault)
    // Realce dos grupos
    this.svg
      .selectAll(".group-path")
      .filter(dd => dd.index !== d.source.index && dd.index !== d.target.index && dd.visibility === true)
      .transition()
      .style("fill-opacity", enable ? 0.1 : this.opacityDefault)
    // Realce do texto
    this.svg
      .selectAll(".group-text")
      .filter(dd => dd.index !== d.source.index && dd.index !== d.target.index && dd.visibility === true)
      .transition()
      .style("fill-opacity", enable ? 0.1 : this.opacityDefault)
  }

  clear() {
    if (!this.container.empty()){
      this.container.html("")
    }
  }
  
  setSize(w, h){
    this.width = w
    this.height = h
    this.innerRadius = Math.min(this.width, this.height) * 0.5 - 90
    this.outerRadius = this.innerRadius + 8
    
    this.ribbon = d3.ribbonArrow()
      .radius(this.innerRadius - 0.5)
      .padAngle(1 / this.innerRadius)
    
    this.arc = d3.arc()
      .innerRadius(this.innerRadius)
      .outerRadius(this.outerRadius)
  }

  update (newData) {
    this.groupNames = undefined
    this.colorScale = undefined
    this.chords = undefined
    this.data = newData
    this.chords = this.generateChords(newData)

    // Remove eventos
    this.svg.selectAll(".ribbons")
      .on("mouseover", null)
      .on("mouseout", null)
    
    this.svg.selectAll(".group")
      .on("mouseover", null)
      .on("mouseout", null)

    // Remover elementos sem dados
    this.svg
      .select(".ribbons-group")
      .selectAll(".ribbons")
      .data(this.chords, c => c.fromTo)
      .exit()
      .transition()
      .duration(500)
      .attrTween("opacity", () => d3.interpolate(1, 0))
      .remove()

    this.svg
      .selectAll(".group")
      .data(this.chords.groups, c => c.name)
      .exit()
      .transition()
      .duration(500)
      .attrTween("opacity", () => d3.interpolate(1, 0))
      .remove()
    
    // Adicionar novos elementos
    this.svg.select(".ribbons-group")
      .selectAll(".ribbons")
      .data(this.chords, c => c.fromTo)
      .enter()
      .each(d => (d.visibility = true))
      .append("path")
      .on("mouseover", (e, d) => {
        if(d.visibility === true) {
          // Mostra tooltip
          this.tooltipControl (true, e, this.numberSeparatorFormat(d.source.value))
          // Ativa highlight
          this.highlightRibbon(true, d)
        }
      })
      .on("mouseout", (e,d) => {
        if(d.visibility === true) {
          // Mostra tooltip
          this.tooltipControl (false)
          // Desativa highlight
          this.highlightRibbon(false, d)
        }
      })
      .attr("pointer-events", "none")
      .each(d => this.ribbonPosition.set(d.fromTo, {
        source: {startAngle: d.source.startAngle, endAngle: d.source.endAngle, radius: d.source.radius},
        target: {startAngle: d.target.startAngle, endAngle: d.target.endAngle, radius: d.target.radius}
      }))
      .transition()
      .duration(500)
      .attrTween("opacity", () => d3.interpolate(0.1, 1))
      .on("end", function (d) { 
        d3.select(this)
        .attr("pointer-events", "all")
      })
      .attr("class", "ribbons")
      .attr("fromTo", c => c.fromTo)
      .attr("d", this.ribbon)
      .each(function(d) { this._current = d })
      .attr("fill", d => this.getColorScale(this.getGroupNames(d.target.index)))
      .attr("stroke", d => d3.rgb(this.getColorScale(this.getGroupNames(d.target.index))).darker())
      .attr("stroke-width", "0.3px")
    
    const groupInsert = this.svg
      .select(".group-group").selectAll(".group")
      .data(this.chords.groups, c => c.name)
      .enter()
      .each(d => (d.visibility = true))
      .append("g")
      .attr("class", "group")
      .on("mouseover", (e, d) => {
        if(d.visibility === true) {
          // Mostra tooltip
          this.tooltipControl(true, e, this.numberSeparatorFormat(d.value))
          // Ativa highlight
          this.highlightGroup(true, d)
        }
      })
      .on("mouseout", (e, d) => {
        if(d.visibility === true) {
          // Oculta tooltip
          this.tooltipControl(false)
          // Desativa highlight
          this.highlightGroup(false, d)
        }
      })
       
    groupInsert.append("path")
      .transition()
      .duration(500)
      .attrTween("opacity", () => d3.interpolate(0.1, 1))
      .attr("class", "group-path")
      .attr("name", c => c.name)
      .attr("fill", d => this.getColorScale(this.getGroupNames(d.index)))
      .attr("d", this.arc)
      .each(d => this.arcPosition.set(d.name, {
        startAngle: d.startAngle,
        endAngle: d.endAngle,
      }))
      .each(function(d) { this._current = d; })
      .attr("id", (d, i) => "group" + d.index) //add id 
    
    groupInsert.append("text")
      .attr("class", "group-text")
      .each(d => {
        d.angle = (d.startAngle + d.endAngle) / 2
        this.textPosition.set(this.getGroupNames(d.index), d.angle)
      })
      .attr("dy", "0.35em")
      .transition()
      .duration(500)
      .attrTween("transform", d => this.textTweent(d, this))
      .attr("text-anchor", d => d.angle > Math.PI ? "end" : null)
      .each(function(d) { this._current = d.angle; })
      .text(d => this.getGroupNames(d.index))

    // Atualizar elementos
    const upRibbon = new Promise(res => {
      this.svg
      .select(".ribbons-group")
        .selectAll(".ribbons")
        .data(this.chords, c => c.fromTo)
          .on("mouseover", (e, d) => {
            if(d.visibility === true) {
              // Mostra tooltip
              this.tooltipControl (true, e, this.numberSeparatorFormat(d.source.value))
              // Ativa highlight
              this.highlightRibbon(true, d)
            }
          })
          .on("mouseout", (e,d) => {
            if(d.visibility === true) {
              // Mostra tooltip
              this.tooltipControl (false)
              // Desativa highlight
              this.highlightRibbon(false, d)
            }
          })
        .attr("pointer-events", "none")
        .transition()
        .duration(500)
        .attrTween("d", d => this.chordTween(d, this))
        .on("end", function (d) { 
          d3.select(this)
          .attr("pointer-events", "all")
          res(true)
        })
        .attr("fill", d => this.getColorScale(this.getGroupNames(d.target.index)))
        .attr("stroke", d => d3.rgb(this.getColorScale(this.getGroupNames(d.target.index))).darker())
      })

    this.svg
    .selectAll(".group")
      .data(this.chords.groups, c => c.name)
      .each(d => (d.visibility = true))
      .on("mouseover", (e, d) => {
        if(d.visibility === true) {
          // Mostra tooltip
          this.tooltipControl(true, e, this.numberSeparatorFormat(d.value))
          // Ativa highlight
          this.highlightGroup(true, d)
        }
      })
      .on("mouseout", (e, d) => {
        if(d.visibility === true) {
          // Oculta tooltip
          this.tooltipControl(false)
          // Desativa highlight
          this.highlightGroup(false, d)
        }
      }) 
    const upPath = new Promise(res => {
      this.svg
      .selectAll(".group-path")
        .data(this.chords.groups, c => c.name)
        .transition()
        .duration(500)
        .attrTween("d", a => this.arcTween(a, this))
        .on("end", () => res(true))
    })

    const upText = new Promise(res => {
      this.svg
      .selectAll("text")
        .data(this.chords.groups, c => c.name)
        .each(d => (d.angle = (d.startAngle + d.endAngle) / 2))
        .transition()
        .duration(500)
        .attrTween("transform", d => this.textTweent(d, this))
        .attr("text-anchor", d => d.angle > Math.PI ? "end" : null)
        .on("end", () => res(true))
    })

    return Promise.all([upPath, upText])
  }
  
  textTweent(t, chart) {
    const name = chart.getGroupNames(t.index)
    const previous = chart.textPosition.get(name)
    const i = d3.interpolate(previous, t.angle)

    chart.textPosition.set(name, t.angle)

    return function(t) {
      return `rotate(${(i(t) * 180 / Math.PI - 90)})
        translate(${chart.outerRadius + 5})
        ${i(t) > Math.PI ? "rotate(180)" : ""}
      `
    }
  }

  arcTween(a, chart) {
    const previous = chart.arcPosition.get(a.name)
    const i = d3.interpolate(previous, a);
    
    chart.arcPosition.set(a.name, {
      startAngle: a.startAngle,
      endAngle: a.endAngle,
    })

    return function(t) {
      return chart.arc(i(t));
    }
  }

  chordTween(c, chart) {
    const previous = chart.ribbonPosition.get(c.fromTo)
    const i = d3.interpolate(previous, c)
    
    this.ribbonPosition.set(c.fromTo, {
      source: {startAngle: c.source.startAngle, endAngle: c.source.endAngle, radius: c.source.radius},
      target: {startAngle: c.target.startAngle, endAngle: c.target.endAngle, radius: c.target.radius}
    })

    return function(t) {
      return chart.ribbon(i(t));
    }
  }

  setGroupVisibility(groupName) {
    this.svg.select(".ribbons-group")
      .selectAll(".ribbons")
      .data(this.chords, c => c.fromTo)
      .transition()
      .duration(500)
      .style("fill-opacity", d => {
        if (groupName && !groupName.includes(this.groupNames[d.source.index]) && !groupName.includes(this.groupNames[d.target.index])) {
          d.visibility = false
          return '0.1'
        }
        else {
          d.visibility = true
          return this.opacityDefault
        }
      })
      .style("stroke-opacity", d => {
        if (groupName && !groupName.includes(this.groupNames[d.source.index]) && !groupName.includes(this.groupNames[d.target.index])) {
          return '0.1'
        }
        else {
          return this.opacityDefault
        }
      })
    
    this.svg
    .selectAll(".group-path")
      .data(this.chords.groups, c => c.name)
      .transition()
      .duration(500)
      .style("fill-opacity", d => {
        if (groupName && !groupName.includes(d.name)) {
          d.visibility = false
          return '0.1'
        }
        else {
          d.visibility = true
          return this.opacityDefault
        }
      })
  }

  highlightLag(){
    const visibleGroups = []
    const rbPromise = new Promise(res => {
      this.svg.select(".ribbons-group")
        .selectAll(".ribbons")
        .data(this.chords, c => c.fromTo)
        .transition()
        .duration(500)
        .on("end", () => {
          res(true)
        })
        .style("fill-opacity", d => {
          if (this.recommendedStage.get(this.getGroupNames(d.source.index)) === this.getGroupNames(d.target.index)) {
            d.visibility = false
            return 0.1
          }
          else {
            // Armazena os grupos que devem ficar visíveis
            if (!visibleGroups.includes(d.source.index)){
              visibleGroups.push(d.source.index)
            }
            if (!visibleGroups.includes(d.target.index)){
              visibleGroups.push(d.target.index)
            }
            d.visibility = true
            return this.opacityDefault
          }
        })
        .style("stroke-opacity",  d => {
          if (this.recommendedStage.get(this.getGroupNames(d.source.index)) === this.getGroupNames(d.target.index)) {
            return 0.1
          }
          else {
            return this.opacityDefault
          }
        })
      })
    const gpPromise = new Promise(res => {
      this.svg
        .selectAll(".group-path")
          .data(this.chords.groups, c => c.name)
        .transition()
        .duration(500)
        .on("end", () => {
          res(true)
        })
        .style("fill-opacity", d => {
          if (visibleGroups.includes(d.index)) {
            return this.opacityDefault
          }
          else {
            d.visibility = false
            return 0.1
          }
        })
      })
    return Promise.all([rbPromise,gpPromise])
  }

  numberSeparatorFormat(num) {
    return d3.format(",.0f")(num).replace(/,/g, '.')
  }
}