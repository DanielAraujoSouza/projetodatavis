class ChoroplethMapChart {
  constructor (containerID = "", opc) {
    this.data = undefined
    this.containerID = (containerID) ? containerID : undefined
    this.mapInstance =  undefined
    this.infoControl = undefined
    this.legendControl = undefined
    this.geoj = undefined
    this.colorScale = undefined
    this.legendIntv = undefined
    this.layers = []
    this.charId = (new Date()).getTime()
    // Opções
    this.numIntv = (opc && opc.hasOwnProperty('numIntv')) ? opc.numIntv : 7
    this.infoTitle = (opc && opc.hasOwnProperty('infoTitle')) ? opc.infoTitle : ''
    this.infoSubtitle = (opc && opc.hasOwnProperty('infoSubtitle')) ? opc.infoSubtitle : ''
    this.valueSuffix = (opc && opc.hasOwnProperty('valueSuffix')) ? opc.valueSuffix : ''
    this.zoomLevel = (opc && opc.hasOwnProperty('zoomLevel')) ? opc.zoomLevel : 2
    this.numFormat = (opc && opc.hasOwnProperty('numFormat')) ? opc.numFormat : d3.format(".0d")
    this.scale = (opc && opc.hasOwnProperty('scale')) ? opc.scale : d3.scaleLinear()
    this.colorInterpolator = (opc && opc.hasOwnProperty('colorInterpolator')) ? opc.colorInterpolator : d3.interpolateBlues
    
  }
  
  build (data, geoData, newContainerID, opc) {
    if (newContainerID) {
      this.containerID = newContainerID
    }
    // Atualiza opções
    if(opc) {
      this.setOptions(opc)
    }
    
    // Armazena os dados
    this.data = data
    this.geoData = geoData
    
    // Gera uma intancia de mapa
    this.mapInstance = L.map(this.containerID) //.setView(this.getCenterMap(), 12)
    this.fitToView()
    const currentZoomLevel = this.mapInstance.getZoom()
    // Define o tipo de mapa
    L.tileLayer("https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png", {
      attribution: `&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, Map tiles by &copy; <a href="https://carto.com/attribution">CARTO</a>`,
      maxZoom: currentZoomLevel + this.zoomLevel,
      minZoom: currentZoomLevel - this.zoomLevel
    }).addTo(this.mapInstance)
    
    // GeoJson
    this.geoj = L.geoJson(this.geoData, {
				style: (feature) => {
          //console.log()
          return this.featureStyle(feature)
        },
				onEachFeature: (feature,layer) => {
          this.layers.push(layer)
          if (this.featureValue(feature.properties.featureName) !== undefined) {
            this.onEachFeature(layer)
          }
        }
		}).addTo(this.mapInstance)
    
    // Informação
    this.infoControl = L.control()

    this.infoControl.onAdd = function (map) {
      this._div = L.DomUtil.create('div', 'info');
      this.update();
      return this._div;
    }
    this.infoControl.update = (feat) => {
      this.infoControl._div.innerHTML = `${this.infoTitle}${feat ? `<b>${feat.properties.featureName}</b><br/>${this.numFormat(this.featureValue(feat.properties.featureName))}  ${this.valueSuffix}` : this.infoSubtitle}`
    }

    this.infoControl.addTo(this.mapInstance);
    
    // Legend Control
    this.legendControl = L.control({position: 'bottomright'});

    this.legendControl.onAdd = () => {
      const div = L.DomUtil.create('div', 'info legend d-flex flex-column')
      const labels = []
      
      this.getLegendIntv().map(intv => {
        const intvColor = this.getColorScale(intv[0])
        const start = this.numFormat(intv[0])
        const end = this.numFormat(intv[1])
        
        labels.push(`<div class="row mx-1"><i style="background:${intvColor}"></i> ${start}&ndash;${end}</div>`)
      })

      div.innerHTML = labels.join('')
      return div
    }

      this.legendControl.addTo(this.mapInstance)
  }
  
  featureStyle(feature) {
    const featureValue = this.featureValue(feature.properties.featureName)
    if(featureValue !== undefined) {
      return {
        weight: 1,
        opacity: 1,
        color: 'white',
        dashArray: '3',
        fillOpacity: 0.6,
        fillColor: this.getColorScale(featureValue)
      }
    }
    else {
      return {
        weight: 1,
        opacity: 1,
        color: 'white',
        fillColor: "#d3d3d3",
        fillOpacity: 0.6,
        dashArray: '',
        fillOpacity: 0.2
      }
    }
  }
  
  onEachFeature(layer) {
		layer.on({
      mouseover: (e) => {
        const layer = e.target
        layer.setStyle({
          weight: 2,
          color: '#AAA',
          dashArray: '',
          fillOpacity: 0.7
        })

        if (!L.Browser.ie && !L.Browser.opera) {
          layer.bringToFront();
        }
        this.infoControl.update(layer.feature);
      },
      mouseout: (e) => {
        this.geoj.resetStyle(e.target);
        this.infoControl.update();
      },
      click: (e) => {
        //console.log(e.target.getBounds())
        this.mapInstance.fitBounds(e.target.getBounds());
      }
    })
	}
  
  getLegendIntv() {
    if (this.legendIntv === undefined) {
      const dataExtent = d3.extent(this.data, d => d.featureValue)
        //console.log(dataExtent)
        const intvScale = this.scale
          .domain([dataExtent[0], dataExtent[1]])
          .range([0, this.numIntv])
        
        this.legendIntv = Array(this.numIntv).fill(0).map((_,i) => [intvScale.invert(i), intvScale.invert(i+1)])
    }
    return this.legendIntv
  }

  getColorScale (featureValue){
    if (this.colorScale === undefined) {
      
      const intvDomain = [...new Set(this.getLegendIntv().reduce((r,v) => r.concat(v)))]
      
      const seqColorScale = d3.scaleSequential(this.colorInterpolator).domain([0,this.numIntv])
      const colors = Array(this.numIntv).fill(0).map((_,i) => seqColorScale(i))
      
      this.colorScale = d3.scaleQuantile()
      .domain(intvDomain)
      .range(colors)
    }
    return featureValue !== undefined ? this.colorScale(featureValue) : this.colorScale
  }
  
  featureValue (featureName) {
    const feature = this.data.find(e => e.featureName === featureName)
    return feature !== undefined ? feature.featureValue : feature
  }
  
  getMapLatLngBounds() {
    if (this.mapLatLngBounds === undefined) {
      const extentCoordinates = this.geoData.features.reduce((r,v) => {
      v.geometry.coordinates[0].forEach(e => {
          // Min lat
          r[0] = d3.min([r[0],e[1]])
          // Max lat
          r[1] = d3.max([r[1],e[1]])
          // Min lang
          r[2] = d3.min([r[2],e[0]])
          // Max lang
          r[3] = d3.max([r[3],e[0]])
        })
        return r
      }, [])

      this.mapLatLngBounds = L.latLngBounds(L.latLng(extentCoordinates[0], extentCoordinates[2]), 
                            L.latLng(extentCoordinates[1], extentCoordinates[3]))
    }
    return this.mapLatLngBounds
  }
  
  fitToView() {
    this.mapInstance.fitBounds(this.getMapLatLngBounds())
  }
  
  update(newData, opc) {
    this.colorScale = undefined
    this.legendIntv = undefined
    this.data = newData
    this.setOptions(opc)
    
    // Atualiza Layers
    this.layers.forEach(layer  => {
      layer.off()
      //console.log(layer)
      layer.setStyle(this.featureStyle(layer.feature))
      if (this.featureValue(layer.feature.properties.featureName) !== undefined) {
        this.onEachFeature(layer)
      }
    })
    
    // Atualiza Legenda
     this.legendControl.onAdd = () => {
      const div = L.DomUtil.create('div', 'info legend')
      const labels = []
      
      this.getLegendIntv().map(intv => {
        const intvColor = this.getColorScale(intv[0])
        const start = this.numFormat(intv[0])
        const end = this.numFormat(intv[1])
        
        labels.push(`<div class="row mx-1"><i style="background:${intvColor}"></i> ${start}&ndash;${end}</div>`)
        
      })

      div.innerHTML = labels.join('')
      return div
    }
    
    this.legendControl.addTo(this.mapInstance)
    
    // Atualiza Informção
    this.infoControl.update = (feat) => {
      this.infoControl._div.innerHTML = `${this.infoTitle}${feat ? `<b>${feat.properties.featureName}</b><br/>${this.numFormat(this.featureValue(feat.properties.featureName))}  ${this.valueSuffix}` : this.infoSubtitle}`
    }

    this.infoControl.addTo(this.mapInstance)
  }
  
  setOptions (opc) {
    if (opc) {
      // Opções
      this.numIntv = opc.numIntv !== undefined ? opc.numIntv : this.numIntv
      this.infoTitle = opc.infoTitle !== undefined ? opc.infoTitle : this.infoTitle
      this.infoSubtitle = opc.infoSubtitle !== undefined ? opc.infoSubtitle : this.infoSubtitle
      this.valueSuffix = opc.valueSuffix !== undefined ? opc.valueSuffix : this.valueSuffix
      this.zoomLevel = opc.zoomLevel !== undefined ? opc.zoomLevel : this.zoomLevel
      this.numFormat = opc.numFormat !== undefined ? opc.numFormat : this.numFormat
      this.scale = opc.scale !== undefined ? opc.scale : this.scale 
      this.colorInterpolator = opc.colorInterpolator !== undefined ? opc.colorInterpolator : this.colorInterpolator
    }
  }
}