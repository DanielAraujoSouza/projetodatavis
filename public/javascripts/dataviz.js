const grafico2 = new ChordChart()
const grafico3 = new ChoroplethMapChart()
const grafico4 = new EdgeBundleChart()
const grafico7 = new ChoroplethMapChart()

const windowLoad = new Promise(function(resolve, reject) {
  window.onload = resolve;
});

// Carrega os dados
const bas_matricula = d3.dsv("|", "/data/BAS_MATRICULA_CE.CSV")
  .then(data => {
    const intFormat = d3.format("d")
    data.forEach(d => {
      d.CO_MUNICIPIO_END = intFormat(d.CO_MUNICIPIO_END)
      d.CO_MUNICIPIO = intFormat(d.CO_MUNICIPIO)
      d.NU_IDADE = +d.NU_IDADE

      const etapa = +d.TP_ETAPA_ENSINO
      if(etapa === 1) {
        d.TP_ETAPA_ENSINO = "Creche"
        if (d.NU_IDADE < 4) {
          d.ETAPA_REGULAR = true
        }
      }
      else if(etapa === 2) {
        d.TP_ETAPA_ENSINO = "Pré-escola"
        if (d.NU_IDADE > 3 &&  d.NU_IDADE < 6) {
          d.ETAPA_REGULAR = true
        }
      }
      else if([4,5,6,7,8,9,10,11,14,15,16,17,18,19,20,21,41].includes(etapa)) {
        d.TP_ETAPA_ENSINO = "Fundamental"
        if (d.NU_IDADE > 5 &&  d.NU_IDADE < 15) {
          d.ETAPA_REGULAR = true
        }
      }
      else if([25,26,27,28,29,35,36,37,38].includes(etapa)) {
        d.TP_ETAPA_ENSINO = "Médio"
        if (d.NU_IDADE > 14 &&  d.NU_IDADE < 18) {
          d.ETAPA_REGULAR = true
        }
      }
      else if([30,31,32,33,34,39,40,68].includes(etapa)) {
        d.TP_ETAPA_ENSINO = "Técnico"
        if (d.NU_IDADE > 14 &&  d.NU_IDADE < 18) {
          d.ETAPA_REGULAR = true
        }
      }
      else if([65,67,69,70,71,72,73,74].includes(etapa)) {
        d.TP_ETAPA_ENSINO = "EJA"
      }
      if (!d.ETAPA_REGULAR) {
        d.ETAPA_REGULAR = false
      }
    })

    return data
  })

const bas_escola =  d3.dsv("|", "/data/BAS_ESCOLA_CE.CSV")

const municipios_ce =  d3.dsv("|", "/data/Municipios_CE.csv")

const indicadores_ce = d3.dsv("|", "/data/Indicadore2016-2020.csv")

const ceara_geojson = d3.json("/data/CearaMunicipios.geojson").then(data => {
  data.features.map(e => (delete Object.assign(e.properties, {['featureName']: e.properties['description'] })['description']))
  return data
})

const idhm_ce = d3.dsv("|", "/data/IDHM_CE_2010.csv")

//// Renderiza os graficos
// Grafico 1
Promise.all([bas_matricula, windowLoad, ]).then(data => {

  $("#load-spinner").remove()
  $("main").removeClass("d-none")

  let i = 1
  data = data[0].map(function(d) {
    d.Dependencia = d.TP_DEPENDENCIA ==="1" ? "Federal":(d.TP_DEPENDENCIA ==="2" ? "Estadual":(d.TP_DEPENDENCIA ==="3" ? "Municipal":(d.TP_DEPENDENCIA ==="4" ? "Privada":"")))
      
    d.Acessibilidade =  d.IN_ACESSIBILIDADE_INEXISTENTE ==="0.0" ? "Não":(d.IN_ACESSIBILIDADE_INEXISTENTE !=="0.0" ? "Sim":"Não Declarada")       
      
    d.Sexo = d.TP_SEXO === "1" ? "Feminino" : "Masculino"
  
    d.Raca = d.TP_COR_RACA === "1" ? "Branca" : (d.TP_COR_RACA === "2" ?  "Preta" : (d.TP_COR_RACA === "3" ? "Parda" :
  (d.TP_COR_RACA === "4" ? "Amarela":(d.TP_COR_RACA === "5" ? "Indígena":(d.TP_COR_RACA === "0" ? "Não_Declarada":"")))))
   d.Zona = d.TP_ZONA_RESIDENCIAL === "1" ? "Rural" : "Urbana"
   d.Alunos = "Total de Matrículas"
   d.chave = i
       i++
    return d
  })

  const facts = crossfilter(data)    
  const dimTotalMat = facts.dimension(d=>d.Alunos)  
  const dimTotalMatGroup = dimTotalMat.group()  
  const dimByZona = facts.dimension(d=>d.Zona)  
  const dimByZonaGroup=dimByZona.group()
  const dimBySexo = facts.dimension(d=>d.Sexo) 
  const dimBySexoGroup = dimBySexo.group() 
  const dimByRaca = facts.dimension(d=>d.Raca)
  const dimByRacaGroup = dimByRaca.group()
  const dimDependencia=facts.dimension(d=>d.Dependencia)
  const dimDependenciaGroup = dimDependencia.group()
  
  const dimRacaNova = facts.dimension(d=>d.TP_COR_RACA)
  
  const dimRacaNovaGroup = dimRacaNova.group().reduce(
        function (p, v) {
              p.Branca += v.TP_COR_RACA === "1";
              p.Preta +=  v.TP_COR_RACA === "2";  
              p.Parda +=  v.TP_COR_RACA === "3"
              p.Amarela += v.TP_COR_RACA === "4"
              p.Indigena += v.TP_COR_RACA === "5"
              p.NaoDeclarada += v.TP_COR_RACA === "0"
              return p;
          },
          function (p, v) {
              p.Branca -= v.TP_COR_RACA === "1";
              p.Preta -=  v.TP_COR_RACA === "2";  
              p.Parda -=  v.TP_COR_RACA === "3"
              p.Amarela -= v.TP_COR_RACA === "4"
              p.Indigena -= v.TP_COR_RACA === "5"
              p.NaoDeclarada -= v.TP_COR_RACA === "0"         
              return p;
          },
          function () {
              return {
              Branca :0,
              Preta :0,  
              Parda :0,
              Amarela :0,
              Indigena :0,
              NaoDeclarada  :0,            
              };
          }
  )
  
  dataNovo = dimRacaNovaGroup.all().map(d=>d.key),dimRacaNovaGroup.all().map(d=>d.value)
  const reds = d3.schemeReds[5]
  const greens = d3.schemePurples[5]
  
  const bcr = d3.select("#g-1-1").node().getBoundingClientRect()
  const width = bcr.width
  const height = bcr.height
  const radius = width/3
  const innerRadius = radius - 40
      
  const pie1 =  dc.pieChart(("#g-1-1"))
  const pie2 =  dc.pieChart(("#g-1-2"))
  const pie3 =  dc.pieChart(("#g-1-3"))
  const pie4 =  dc.pieChart(("#g-1-4"))
  const pie5 =  dc.pieChart(("#g-1-5"))
        pie1.width(width)
            .height(height)
            .radius(radius)
            .renderLabel(true)          
            .slicesCap(1)
            .innerRadius(innerRadius)
            .externalLabels(-200)
            .externalRadiusPadding(10)
            .drawPaths(true)
            .dimension(dimTotalMat)
            .group(dimTotalMatGroup)          
            .legend(dc.legend())          
    
    pie1.on('pretransition', function(pie2) {
            pie1.selectAll('.dc-legend-item text')
                .text('')
              .append('tspan')
                .text(function(d) { return d.name + " : " +d.data; })
                .style("font-family","Arial")
                .style("font-size","14px")
                .style("font-weight", "bold")
              .append('tspan')
                .attr('x', 0)
                
      
        pie1.selectAll('text.pie-slice').text(function(d) {
              return d.data.key + " " +d.data.value;
          })
         });
    
    
    
        pie2.width(width)
            .height(height)
            .radius(radius)
            .colors(d3.scaleOrdinal ([ "#a50f15", "#de2d26", "#fb6a4a","#fc9272", "#fcbba1", "#fee5d9"]) )           
            .slicesCap(6)
            .innerRadius(innerRadius)
            .externalLabels(-100)
            .externalRadiusPadding(-0.1)
            .drawPaths(true)
            .dimension(dimByRaca)
            .group(dimByRacaGroup)
            .legend(dc.legend())
            .minAngleForLabel()
            
    
    pie2.on('pretransition', function(pie2) {
            pie2.selectAll('.dc-legend-item text')                 
                .text('')              
              .append('tspan')
                .text(function(d) { return d.name + " : " +d.data;})
                .style("font-family","Arial")
                .style("font-size","14px")
                .style("font-weight", "bold")                                                 
                
            pie2.selectAll('text.pie-slice').text(function(d) {
                 if((d.endAngle - d.startAngle)>0.5)
              return d.data.key + ' ' + dc.utils.printSingleValue((d.endAngle - d.startAngle) / (2*Math.PI) * 100) + '%';
          })
        });
    
        pie3.width(width)
            .height(height)
            .radius(radius)
            .colors(d3.scaleOrdinal (["pink","blue"]))
            .slicesCap(4)
            .innerRadius(innerRadius)
            .externalLabels(-100)
            .externalRadiusPadding(0.1)
            .drawPaths(true)
            .dimension(dimBySexo)
            .group(dimBySexoGroup)          
            .legend(dc.legend()) 
    
      pie3.on('pretransition', function(chart) {
            pie3.selectAll('.dc-legend-item text')
                .text('')
              .append('tspan')
                .text(function(d) { return d.name + " : "+d.data; })
                .style("font-family","Arial")
                .style("font-size","14px")
                .style("font-weight", "bold")
              .append('tspan')
                .attr('x', 100)
                .attr('text-anchor', 'end')
                //.text(function(d) { return d.data; });
        
             pie3.selectAll('text.pie-slice').text(function(d) {
              return d.data.key + ' ' + dc.utils.printSingleValue((d.endAngle - d.startAngle) / (2*Math.PI) * 100) + '%'; })
        });
    
        pie4.width(width)
            .height(height)
            .radius(radius)
            .colors(d3.scaleOrdinal (["green","grey"]))
            .slicesCap(4)
            .innerRadius(innerRadius)
            .externalLabels(-100)
            .externalRadiusPadding(0.1)
            .drawPaths(true)
            .dimension(dimByZona)
            .group(dimByZonaGroup)          
            .legend(dc.legend())
    
      pie4.on('pretransition', function(pie4) {
            pie4.selectAll('.dc-legend-item text')
                .text('')
              .append('tspan')
                .text(function(d) { return d.name + " :" + d.data; })
                .style("font-family","Arial")
                .style("font-size","14px")
                .style("font-weight", "bold")
              .append('tspan')
                .attr('x', 100)
                .attr('text-anchor', 'end')
                //.text(function(d) { return d.data; });
        
             pie4.selectAll('text.pie-slice').text(function(d) {
              return d.data.key + ' ' + dc.utils.printSingleValue((d.endAngle - d.startAngle) / (2*Math.PI) * 100) + '%'; })
        });
    
        pie5.width(width)
            .height(height)
            .radius(radius)
            .colors(d3.scaleOrdinal (["#54278f", "#756bb1","#9e9ac8","#cbc9e2"  ]))
            .slicesCap(4)
            .innerRadius(innerRadius)
            .externalLabels(-90)
            .minAngleForLabel(0.3)
            .externalRadiusPadding(0.1)
            .drawPaths(true)
            .dimension(dimDependencia)
            .group(dimDependenciaGroup)          
            .legend(dc.legend()) 
    
      pie5.on('pretransition', function(pie5) {
            pie5.selectAll('.dc-legend-item text')
                .text('')
              .append('tspan')
                .text(function(d) { return d.name + "  :" +d.data; })
                .style("font-family","Arial")
                .style("font-size","14px")
                .style("font-weight", "bold")
              .append('tspan')
                .attr('x', 100)
                .attr('text-anchor', 'end')
                //.text(function(d) { return d.data; });
        
             pie5.selectAll('text.pie-slice').text(function(d) {
               if((d.endAngle - d.startAngle)>0.9)
              return d.data.key + ' ' + dc.utils.printSingleValue((d.endAngle - d.startAngle) / (2*Math.PI) * 100) + '%'; })
             
        });  
    
    dc.renderAll()  
})

// Grafico 2
Promise.all([bas_matricula, windowLoad]).then(data => {
  const g2Data = data[0]
  const g2DataLag = g2Data.filter(d => d.ETAPA_REGULAR === false)

  let g2Status = ''

  // Transição 1
  grafico2.build(g2Data, "#g-2")

  inViewPort("#g-2-t1-0", () => {
    if (g2Status === 'g-2-t1-1') {
      grafico2.setGroupVisibility()
    }
    g2Status = 'g-2-t1-0'
  })
  inViewPort("#g-2-t1-1", () => {
    g2Status = 'g-2-t1-1'
    grafico2.setGroupVisibility(["Pré-escola", "Creche"])
  })
  inViewPort("#g-2-t1-2", () => {
    g2Status = 'g-2-t1-2'
    grafico2.setGroupVisibility(["Fundamental"])
  })
  inViewPort("#g-2-t1-3", () => {
    g2Status = 'g-2-t1-3'
    grafico2.setGroupVisibility(["Médio"])
  })
  inViewPort("#g-2-t1-4", () => {
    g2Status = 'g-2-t1-4'
    grafico2.setGroupVisibility(["Técnico"])
  })
  inViewPort("#g-2-t1-5", () => {
    g2Status = 'g-2-t1-5'
    grafico2.setGroupVisibility(["EJA"])
  })

  // Transição 2
  inViewPort("#g-2-t2-0", async () => {
    
    if (g2Status === 'g-2-t2-1') {
      await grafico2.update(g2Data)
    }
    else {
      grafico2.setGroupVisibility()
    }

    g2Status = 'g-2-t2-0'
    await grafico2.highlightLag()
  })
  
  inViewPort("#g-2-t2-1", async () => {
    if (g2Status === 'g-2-t2-0') {
      await grafico2.update(g2DataLag)
    }
    g2Status = 'g-2-t2-1'
    grafico2.setGroupVisibility(["Pré-escola", "Creche"])
  })
  inViewPort("#g-2-t2-2", async () => {
    g2Status = 'g-2-t2-2'
    grafico2.setGroupVisibility(["18+ anos"])
  })
})

// Grafico 3
Promise.all([bas_matricula, municipios_ce, ceara_geojson, windowLoad]).then(data => {
  let g3Status = 'g-3-t1-0'

  // Dados Municipais
  const municipiosCE = new Map(data[1].map(e => [+e.COD_MUNI, e]))

  // Matriculas em Escolas publicas
  const g3Data = data[0].filter(d => (+d.TP_DEPENDENCIA > 0 && +d.TP_DEPENDENCIA < 4 && municipiosCE.has(+d.CO_MUNICIPIO)))
  
  const studentsCounty = g3Data.reduce((r,v) => {
    arraySumIncrement(v.CO_MUNICIPIO, r)
    return r
  }, [])

  const g3DataT1 = studentsCounty.map(d => {
    const county = municipiosCE.get(+d.key)
    return {
      featureName: county.NOME_MUNI,
      featureValue: d.sum*100000/+county.POPULACAO
    }
  })

  //
  const g3DataT2 = g3Data.filter(d => +d.IN_TRANSPORTE_PUBLICO === 1)
    .reduce((r,v) => {
      arraySumIncrement(v.CO_MUNICIPIO, r)
      return r
    }, []).map(d => {
      const county = municipiosCE.get(+d.key)
      return {
        featureName: county.NOME_MUNI,
        featureValue: d.sum/+county.POPULACAO
      }
    })

  // GeoJson
  const mapGeoJson = data[2]
  
  // Constroi o gráfico
  grafico3.build(g3DataT1, mapGeoJson, "g-3", {
    numIntv: 5,
    infoTitle: '<h5>Matrículas por 100 mil Habitantes</h5>',
    infoSubtitle: 'Passe o mouse sobre um município',
    valueSuffix: 'matrículas',
    zoomLevel: 2,
    numFormat: d3.format(".0d"),
    scale: d3.scalePow().exponent(0.5),
    colorInterpolator: d3.interpolateBlues
  })

  // Update
  inViewPort("#g-3-t1-0", async () => {
    if (g3Status !== 'g-3-t1-0') {
      grafico3.update(g3DataT1, {
        infoTitle: '<h5>Matrículas por 100 mil Habitantes</h5>',
        valueSuffix: 'matrículas',
        numFormat: d3.format(".0d"),
        scale: d3.scalePow().exponent(1),
        colorInterpolator: d3.interpolateBlues
      })
    }
    g3Status = 'g-3-t1-0'
  })

  inViewPort("#g-3-t1-1", async () => {
    if (g3Status !== 'g-3-t1-1') {
      grafico3.update(g3DataT2, {
        infoTitle: '<h5>Percentual por Município</h5>',
        valueSuffix: 'dos alunos',
        numFormat: d3.format(".2%"),
        scale: d3.scalePow().exponent(0.5),
        colorInterpolator: d3.interpolateReds
      })
    }
    g3Status = 'g-3-t1-1'
  })
})

// Grafico 4
Promise.all([bas_matricula, municipios_ce, windowLoad]).then(data => {
  let g4Status = ''
  const codMuniToMEso = new Map(data[1].map(e => [e.COD_MUNI, [e.NOME_MUNI, e.NOME_MESO]]))
  // Formata os dados para o grafico 3
  const g4Data = data[0].reduce((retArr, e) => {
    const from = codMuniToMEso.get(e.CO_MUNICIPIO_END)
    const to = codMuniToMEso.get(e.CO_MUNICIPIO)
    if (from && to && e.CO_MUNICIPIO_END !== e.CO_MUNICIPIO) {
      retArr.push({leaf: from[0], group: from[1], link: to[0], linkGroup: to[1]})
    }
    return retArr
  },[])

  const g4DataMetro = g4Data.filter(e => e.group === "Metropolitana de Fortaleza" || e.linkGroup === "Metropolitana de Fortaleza")
  const g4DataFortal = g4DataMetro.filter(e => e.leaf === "Fortaleza" || e.link === "Fortaleza")
  
  const leavesGroupData = data[1].map(e => ({leaf: e.NOME_MUNI, group: e.NOME_MESO}))

  grafico4.build(g4Data, leavesGroupData, "#g-4")
  inViewPort("#g-4-t1-0", async () => {
    if (g4Status === 'g-2-t2-1') {
      grafico4.update(g4Data)
    }
    g4Status = 'g-2-t2-0'
  })
  inViewPort("#g-4-t1-1", async () => {
    g4Status = 'g-2-t2-1'
    grafico4.update(g4DataMetro)
  })
  inViewPort("#g-4-t1-2", async () => {
    g4Status = 'g-2-t2-2'
    grafico4.update(g4DataFortal)
    $("#g-4-t1-2-select").on('change', function() {
      grafico4.update(g4Data.filter(e => e.leaf === this.value || e.link === this.value))
    })
  })
})

// Grafico 5
Promise.all([bas_escola, windowLoad, bas_matricula]).then(data => {
  let i = 1
  data = data[0].map(function(d) {
    d.Dependencia = d.TP_DEPENDENCIA ==="1" ? "Federal":(d.TP_DEPENDENCIA ==="2" ? "Estadual":(d.TP_DEPENDENCIA ==="3" ? "Municipal":(d.TP_DEPENDENCIA ==="4" ? "Privada":"")))
    d.Acessibilidade =  d.IN_ACESSIBILIDADE_INEXISTENTE ==="0.0" ? "Não":(d.IN_ACESSIBILIDADE_INEXISTENTE !=="0.0" ? "Sim":"Não Declarada")    
    d.Total_de_Escolas = "Total de Escolas"
    d.chave = i
    i++
    return d
  })
  
  const bcr = d3.select("#g-5-1").node().getBoundingClientRect()
  const width = bcr.width
  const height = 150

  const facts = crossfilter(data)
  const xDim = facts.dimension(d => d.Dependencia)
  const xDimGroup = xDim.group().reduceCount()
  const acessibDim = facts.dimension(d => d.Acessibilidade)
  const acessibDimGroup = acessibDim.group().reduceCount()
  const fatiaTotal = facts.dimension(d => d.Total_de_Escolas)
  const fatiaTotalGroup = fatiaTotal.group().reduceCount()

  const cor = d3.schemeCategory10
  const reds = d3.schemeReds[4]
  const colorAcess = d3.scaleOrdinal()
    .domain(["Não", "Sim"])
    .range(["#bcbd22", "#17becf"])
  const colorDep = d3.scaleOrdinal()
    .domain(["Municipal", "Federal", "Privada", "Estadual"])
    .range([ "#a50f15", "#de2d26", "#fb6a4a","#fc9272"])

  let sorted = acessibDimGroup.top(Infinity)
  const names2 = sorted.map(d => d.key)

  sorted = xDimGroup.top(Infinity)
  const names1 = sorted.map(d => d.key)

  const bar1 =  dc.rowChart("#g-5-1")
  const bar2 = dc.rowChart("#g-5-2")
  const bar3 =  dc.rowChart("#g-5-3")

  bar1.width(width)
    .height(height)
    .margins({top: 50, right: 10, bottom: 25, left: 60})
    .x(d3.scaleLinear().domain([fatiaTotal.bottom(1)[0].chave,fatiaTotal.top(1)[0].chave]))
    .legend(dc.legend())
    .label(function (d) {return d.key + " : " + d.value;})
    .dimension(fatiaTotal)
    .elasticX(true)
    .group(fatiaTotalGroup)
    .label(function (d) {return d.key + " : " + d.value;})
    .xAxis().ticks(5)

  bar2.width(width)
    .height(1.5*height)
    .margins({top: 50, right: 10, bottom: 25, left: 60})
    .x(d3.scaleLinear().domain([fatiaTotal.bottom(1)[0].chave,fatiaTotal.top(1)[0].chave]))
    .legend(dc.legend() )
    .colors(colorAcess)
    .colorAccessor(d => d.key)
    .dimension(acessibDim)
    .elasticX(true)
    .group(acessibDimGroup)
    .keyAccessor(function (d) {return d.key;})
    .valueAccessor( function(d) {
      return  d.value;
    })
    .legend(dc.legend(acessibDimGroup))
    .label(function (d) {return d.key + " : " + d.value;})
    .ordering(function(d) { return -d.value })
    .xAxis().tickFormat(function(v){return v}).ticks(5);

  bar3.width(width)
    .height(2.5*height)
    .margins({top: 50, right: 10, bottom: 25, left: 60})
    .legend(dc.legend() )
    .x(d3.scaleLinear().domain([fatiaTotal.bottom(1)[0].chave,fatiaTotal.top(1)[0].chave]))
    .colors(colorDep)
    .colorAccessor(d => d.key)
    .dimension(xDim)
    .elasticX(true)
    .group(xDimGroup)
    .label(function (d) {return d.key + " : " + d.value;})
    .ordering(function(d) { return -d.value })
    .xAxis().tickFormat(function(v){return v}).ticks(7);

  dc.renderAll()
})

// Grafico 6
Promise.all([idhm_ce, bas_escola, windowLoad, bas_matricula]).then(data => {
  const svgContainer = d3.select("#g-6-1")
  const bcr = svgContainer.node().getBoundingClientRect()
  const width = bcr.width < 600 ? 600 : bcr.width
  const height = width/2.5
  const margin = {top: 25, right: 20, bottom: 50, left: 40}

  // IDHM
  const idhm = new Map(data[0].map(d => [d.COD_MUN, {nome: d.MUNICIPIO, idhm: d.IDHM}]))
  
  // Bas_escola
  const baseEscolas = data[1].map(d => {
    if (d.IN_ACESSIBILIDADE_INEXISTENTE == 1.0 || d.IN_ACESSIBILIDADE_INEXISTENTE == "") {
      d.IN_ACESSIBILIDADE_INEXISTENTE = 0
    }
    else if (d.IN_ACESSIBILIDADE_INEXISTENTE == 0.0) {
      d.IN_ACESSIBILIDADE_INEXISTENTE = 1
    }
    return d
  })

  const facts = crossfilter(baseEscolas)
  const municipioDim = facts.dimension(d => d.CO_MUNICIPIO)
  const acessibilidadeGroup = municipioDim.group().reduceSum(d => d.IN_ACESSIBILIDADE_INEXISTENTE)
  const todasEscolasGroup = municipioDim.group().reduceCount(d => d.IN_ACESSIBILIDADE_INEXISTENTE)
  const acessibilidadeMap = new Map(acessibilidadeGroup.all().map(d => [d.key, d.value]))
  const proporcaoEscolas = new Map(todasEscolasGroup.all().map(d => [d.key, {prop: d3.format(".3f")(acessibilidadeMap.get(d.key)/d.value)}]))

  const yAces = d3.scaleLinear()
  .domain([0, d3.max(proporcaoEscolas.values(), d => d.prop)])
  .range([height, 0])

  const xIDHM = d3.scaleLinear()
           .domain([0.53, d3.max(idhm.values(), d => d.idhm)])
           .range([0, width-90])

  const xIDHMAxis = d3.axisBottom().scale(xIDHM)
  
  const yAcesAxis = d3.axisLeft().scale(yAces);

  // Criar Grafico
  const outersvg = svgContainer
      .append("svg")
      .attr("width", width+margin.left+margin.right)
      .attr("height", height+margin.top+margin.bottom)
  
  const svg = outersvg.append('g')
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
  
  // Circulos
  svg.selectAll('circle')
    .data(todasEscolasGroup.all())
    .enter()
    .append('circle')
    .attr('class', "circulos")
    .attr('cx', d => xIDHM(idhm.get(d.key).idhm))
    .attr('cy', d => yAces(proporcaoEscolas.get(d.key).prop))
    .attr('r', 5)
    .attr('fill', 'black')
    .on("mouseenter", function(e, d){
        d3.select(this) // seleciona o elemento atual
          .style("cursor", "pointer") //muda o mouse para mãozinha
          .attr("stroke-width", 2)
          .attr("stroke", "red")
          .attr("fill", "red");

        const rect = this.getBoundingClientRect();
        showTooltip(d.key, e.pageX, e.pageY);
      })
    .on("mouseleave", function(e, d){
        d3.select(this)
          .style("cursor", "default")
          .attr("stroke-width", 0)
          .attr("stroke","none") //volta ao valor padrão
          .attr("fill", 'black')
        hideTooltip();
      });

  // Chamadas para desenhar os eixos
  svg.append('g')
     .attr("transform", "translate(0," + height + ")")
     .call(xIDHMAxis);
  
  svg.append('g')
     .call(yAcesAxis);
  
  svg.append('text')
    .attr("transform", "translate(" + ((width/2)-15) + "," + (height + 30) + ")")
    .style('text-anchor', 'middle')
    .attr('font-family', 'sans-serif')
    .attr('font-size', '11px')
    .text('IDHM');
  
  svg.append('text')
    .attr("transform", "rotate(-90)")
    .attr('y', 0 - 42)
    .attr('x', 0 - (height/2))
    .attr('dy', '1em')
    .style('text-anchor', 'middle')
    .attr('font-family', 'sans-serif')
    .attr('font-size', '11px')
    .text('Escolas com Acessibilidade (Proporção)');
  
  function showTooltip(cod_mun, x, y) {
    const offset = 10;
    const t = d3.select("#tooltip");
    t.select("#taxa").text(d3.format(".0%")(proporcaoEscolas.get(cod_mun).prop));
    t.select("#county-name").text(idhm.get(cod_mun).nome);
    t.classed("hidden", false);
    const rect = t.node().getBoundingClientRect();
    const w = rect.width;
    const h = rect.height;
    if (x + offset + w > width) {
      x = x - w;
    }
    t.style("left", x + offset + "px").style("top", y - h + "px");
  }

  function hideTooltip() {
    d3.select("#tooltip")
    .classed("hidden", true)
  }
})

// Grafico 7
Promise.all([bas_escola, municipios_ce, ceara_geojson, windowLoad, bas_matricula]).then(data => {
  let g7Status = ''
  let schoolsNotWork = []
  const schoolsCounty = []
  const schoolInfra = {
    agua: [],
    esgoto: [],
    biblioteca: [],
    lab_ciencias: [],
    lab_informatica: [],
  }

  data[0].forEach(d => {
    // Somente escolas pública
    if(+d.TP_DEPENDENCIA > 0 && +d.TP_DEPENDENCIA < 4) {
      const countyId = +d.CO_MUNICIPIO
      arraySumIncrement(countyId, schoolsCounty)

      // Escolas públicas que não funcionam
      if(+d.TP_SITUACAO_FUNCIONAMENTO !== 1) {
        arraySumIncrement(countyId, schoolsNotWork)
      }

      // Escolas públicas sem infraestrutura
      let hasInfra = true
      if(+d.IN_AGUA_INEXISTENTE === 1) {
        arraySumIncrement(countyId, schoolInfra.agua)
      }
      if(+d.IN_ESGOTO_INEXISTENTE === 1) {
        arraySumIncrement(countyId, schoolInfra.esgoto)
      }
      if(+d.IN_BIBLIOTECA === 0) {
        arraySumIncrement(countyId, schoolInfra.biblioteca)
      }
      if(+d.IN_LABORATORIO_CIENCIAS === 0) {
        arraySumIncrement(countyId, schoolInfra.lab_ciencias)
      }
      if(+d.IN_LABORATORIO_INFORMATICA === 0) {
        arraySumIncrement(countyId, schoolInfra.lab_informatica)
      }
    }
  })

  // Dados Municipais
  const municipiosCE = new Map(data[1].map(e => [+e.COD_MUNI, e]))

  // Percentual das escolas que não funcionam
  schoolsNotWork = arrayFormat(schoolsNotWork, schoolsCounty, municipiosCE)

  // Percentual de escolas sem infraestrutura
  for (const key in schoolInfra) {
    schoolInfra[key] = arrayFormat(schoolInfra[key], schoolsCounty, municipiosCE)
  }

  // Total escolas publicas por 100 mil
  const g7DataT1 = schoolsCounty.map(d => {
    const county = municipiosCE.get(d.key)
    return {
      featureName: county.NOME_MUNI,
      featureValue: d.sum*100000/+county.POPULACAO
    }
  })

  // GeoJson
  mapGeoJson = data[2]
  
  // Constroi o gráfico
  grafico7.build(g7DataT1, mapGeoJson, "g-7", {
    numIntv: 5,
    infoTitle: '<h5>Escolas por 100 mil Habitantes</h5>',
    infoSubtitle: 'Passe o mouse sobre um município',
    valueSuffix: 'escolas',
    zoomLevel: 2,
    numFormat: d3.format(".1f"),
    scale: d3.scalePow().exponent(0.1),
    colorInterpolator: d3.interpolateBlues
  })

  // Update
  inViewPort("#g-7-t1-0", async () => {
    if (g7Status !== 'g-7-t1-0') {
      grafico7.update(g7DataT1, {
        infoTitle: '<h5>Escolas por 100 mil Habitantes</h5>',
        valueSuffix: 'escolas',
        numFormat: d3.format(".1f"),
        scale: d3.scalePow().exponent(0.1),
        colorInterpolator: d3.interpolateBlues
      })
    }
    g7Status = 'g-7-t1-0'
  })

  inViewPort("#g-7-t1-1", async () => {
    grafico7.update(schoolsNotWork, {
      infoTitle: '<h5>Percentual por Município</h5>',
      valueSuffix: ' das escolas',
      numFormat: d3.format(".0%"),
      scale: d3.scalePow().exponent(1),
      colorInterpolator: d3.interpolateReds
    })
    g7Status = 'g-7-t1-1'
  })

  inViewPort("#g-7-t2-0", async () => {
    grafico7.update(schoolInfra.agua, {
      infoTitle: '<h5>Percentual por Município</h5>',
      valueSuffix: ' das escolas',
      numFormat: d3.format(".1%"),
      scale: d3.scalePow().exponent(0.5),
      colorInterpolator: d3.interpolateGnBu
    })
    g7Status = 'g-7-t2-0'
  })

  inViewPort("#g-7-t2-1", async () => {
    grafico7.update(schoolInfra.esgoto, {
      infoTitle: '<h5>Percentual por Município</h5>',
      valueSuffix: ' das escolas',
      numFormat: d3.format(".1%"),
      scale: d3.scalePow().exponent(0.5),
      colorInterpolator: d3.interpolateGreys
    })
    g7Status = 'g-7-t2-1'
  })

  inViewPort("#g-7-t2-2", async () => {
    grafico7.update(schoolInfra.biblioteca, {
      infoTitle: '<h5>Percentual por Município</h5>',
      valueSuffix: ' das escolas',
      numFormat: d3.format(".0%"),
      scale: d3.scalePow().exponent(1),
      colorInterpolator: d3.interpolateYlOrBr
    })
    g7Status = 'g-7-t2-2'
  })

  inViewPort("#g-7-t2-3", async () => {
    grafico7.update(schoolInfra.lab_ciencias, {
      infoTitle: '<h5>Percentual por Município</h5>',
      valueSuffix: ' das escolas',
      numFormat: d3.format(".0%"),
      scale: d3.scalePow().exponent(1),
      colorInterpolator: d3.interpolateBuPu
    })
    g7Status = 'g-7-t2-3'
  })

  inViewPort("#g-7-t2-4", async () => {
    grafico7.update(schoolInfra.lab_informatica, {
      infoTitle: '<h5>Percentual por Município</h5>',
      valueSuffix: ' das escolas',
      numFormat: d3.format(".1%"),
      scale: d3.scalePow().exponent(1),
      colorInterpolator: d3.interpolatePuBu
    })
    g7Status = 'g-7-t2-4'
  })
  
})

// Grafico 8
Promise.all([indicadores_ce, municipios_ce, windowLoad, bas_matricula]).then(data => {
  const bcr = d3.select("#g-8-1").node().getBoundingClientRect()
  const width = bcr.width
  const height = bcr.width/2.5
  const municipiosMesoMap = new Map(data[1].map(d => [+d.COD_MUNI, d.NOME_MESO]))


  const dataset = data[0].map(d => {
    d.NU_ANO_CENSO = +d.NU_ANO_CENSO
    d.NOME_MESO = municipiosMesoMap.get(+d.CO_MUNICIPIO)
    return d
  })

  const facts = crossfilter(dataset)
  const anoDim = facts.dimension(d => d.NU_ANO_CENSO)
  const propGroup = anoDim.group().reduce(reduceAddAvg, reduceRemoveAvg, reduceInitAvg)

  const xScale = d3.scaleLinear()
           .domain([anoDim.bottom(1)[0].NU_ANO_CENSO - 0.020, anoDim.top(1)[0].NU_ANO_CENSO + 0.020])
  {
    const compositeChart = dc.compositeChart("#g-8-1")
    compositeChart.width(width)
                  .height(300)
                  .margins({top: 70, right: 50, bottom: 25, left: 60})
                  .dimension(anoDim)
                  .x(xScale)
                  .legend(dc.legend().x(width-150).y(5).itemHeight(15).gap(5))
                  .brushOn(false)    
                  .compose([
                    dc.lineChart(compositeChart)
                      .group(propGroup, 'Ceará').valueAccessor(d => d.value.avg).curve(d3.curveCardinal)
                      .ordinalColors(['darkorange'])])
    compositeChart.xAxis()
                  .tickFormat(d3.format("d"))
                  .ticks(5)
    compositeChart
                  .y(d3.scaleLinear().domain([0, 0.9]))
                  .yAxis()
                  .tickFormat(d3.format(".0%"))
                  .ticks(9)
  }
  
  {
    const  meso1 = dataset.filter(d => d.NOME_MESO == "Noroeste Cearense")
    const  meso2 = dataset.filter(d => d.NOME_MESO == "Norte Cearense")
    const  meso3 = dataset.filter(d => d.NOME_MESO == "Metropolitana de Fortaleza")
    const  meso4 = dataset.filter(d => d.NOME_MESO == "Sertões Cearenses")
    const  meso5 = dataset.filter(d => d.NOME_MESO == "Jaguaribe")
    const  meso6 = dataset.filter(d => d.NOME_MESO == "Centro-Sul Cearense")
    const  meso7 = dataset.filter(d => d.NOME_MESO == "Sul Cearense")

    const meso1facts = crossfilter(meso1)
    const meso2facts = crossfilter(meso2)
    const meso3facts = crossfilter(meso3)
    const meso4facts = crossfilter(meso4)
    const meso5facts = crossfilter(meso5)
    const meso6facts = crossfilter(meso6)
    const meso7facts = crossfilter(meso7)

    const anoMeso1Dim = meso1facts.dimension(d => [d.NU_ANO_CENSO])
    const anoMeso2Dim = meso2facts.dimension(d => [d.NU_ANO_CENSO])
    const anoMeso3Dim = meso3facts.dimension(d => [d.NU_ANO_CENSO])
    const anoMeso4Dim = meso4facts.dimension(d => [d.NU_ANO_CENSO])
    const anoMeso5Dim = meso5facts.dimension(d => [d.NU_ANO_CENSO])
    const anoMeso6Dim = meso6facts.dimension(d => [d.NU_ANO_CENSO])
    const anoMeso7Dim = meso7facts.dimension(d => [d.NU_ANO_CENSO])

    const meso1Group = anoMeso1Dim.group().reduce(reduceAddAvg, reduceRemoveAvg, reduceInitAvg)
    const meso2Group = anoMeso2Dim.group().reduce(reduceAddAvg, reduceRemoveAvg, reduceInitAvg)
    const meso3Group = anoMeso3Dim.group().reduce(reduceAddAvg, reduceRemoveAvg, reduceInitAvg)
    const meso4Group = anoMeso4Dim.group().reduce(reduceAddAvg, reduceRemoveAvg, reduceInitAvg)
    const meso5Group = anoMeso5Dim.group().reduce(reduceAddAvg, reduceRemoveAvg, reduceInitAvg)
    const meso6Group = anoMeso6Dim.group().reduce(reduceAddAvg, reduceRemoveAvg, reduceInitAvg)
    const meso7Group = anoMeso7Dim.group().reduce(reduceAddAvg, reduceRemoveAvg, reduceInitAvg)
    
    let compositeChart = dc.compositeChart("#g-8-2")
    compositeChart.width(width)
      .height(300)
      .margins({top: 70, right: 50, bottom: 25, left: 60})
      .dimension(anoDim)
      .x(xScale)
      .legend(dc.legend().x(70).y(10).itemHeight(12).gap(3).horizontal(1).legendWidth(700).itemWidth(200))
      .brushOn(false)    
      .compose([
        dc.lineChart(compositeChart)
          .group(meso1Group, 'Noroeste Cearense').valueAccessor(d => d.value.avg).curve(d3.curveCardinal)
          .ordinalColors(['darkorange']),
        dc.lineChart(compositeChart)
          .group(meso2Group, 'Norte Cearense').valueAccessor(d => d.value.avg).curve(d3.curveCardinal)
          .ordinalColors(['steelblue']),
        dc.lineChart(compositeChart)
          .group(meso3Group, 'Metropolitana de Fortaleza').valueAccessor(d => d.value.avg).curve(d3.curveCardinal)
          .ordinalColors(['green']),
        dc.lineChart(compositeChart)
          .group(meso4Group, 'Sertões Cearenses').valueAccessor(d => d.value.avg).curve(d3.curveCardinal)
          .ordinalColors(['yellow']),
        dc.lineChart(compositeChart)
          .group(meso5Group, 'Jaguaribe').valueAccessor(d => d.value.avg).curve(d3.curveCardinal)
          .ordinalColors(['red']),
        dc.lineChart(compositeChart)
          .group(meso6Group, 'Centro-Sul Cearense').valueAccessor(d => d.value.avg).curve(d3.curveCardinal)
          .ordinalColors(['purple']),
        dc.lineChart(compositeChart)
          .group(meso7Group, 'Sul Cearense').valueAccessor(d => d.value.avg).curve(d3.curveCardinal)
          .ordinalColors(['pink'])])

    compositeChart.xAxis()
      .tickFormat(d3.format("d"))
      .ticks(5)

    compositeChart
      .y(d3.scaleLinear().domain([0, 0.9]))
      .yAxis()
      .tickFormat(d3.format(".0%"))
      .ticks(9)
  }
  

  dc.renderAll()
})

// Obverser 
async function inViewPort(elementId, callback) {
  const observer = new IntersectionObserver(entries => {
    if(entries[0].isIntersecting === true){
      callback()
    }
  }, { threshold: [0] })

  observer.observe(document.querySelector(elementId));
}

function arraySumIncrement(value, array){
  const e = array.find(e => e.key === value)
  if (e) {
    e.sum += 1
  }
  else {
    array.push({key: value, sum: 1})
  }
}

function arrayFormat(srcArr, tgtArr, nameMap) {
  return tgtArr.map(e => {
    const srcElem = srcArr.find(d => e.key === d.key)
    return {
      featureName: nameMap.get(e.key).NOME_MUNI,
      featureValue: (srcElem !== undefined ? srcElem.sum/e.sum : 0)
    }
  })
}

function reduceAddAvg(p,v) {
  p.count += +v.TOTAL_ESCOLA;
  p.sum += +v.TOTAL_ESCOLA_INTERNET;
  p.avg = +d3.format(".3f")(p.sum/p.count);
  
return p;
}

function reduceRemoveAvg(p,v) {
  p.count -= +v.TOTAL_ESCOLA;
  p.sum -= +v.TOTAL_ESCOLA_INTERNET;
  p.avg = p.count ? p.sum/p.count : 0;
  
return p;
}

function reduceInitAvg() {
  return {count:0, sum:0, avg:0};
}