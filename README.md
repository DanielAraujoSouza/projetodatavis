# 2020.2 - VISUALIZAÇÃO DE DADOS - Turma 01 
## **Projeto Final  - Panorama da educação básica no estado do Ceará no ano de 2020**

O objetivo desse projeto é mostrar, de forma simples e intuitiva, um panorama geral a respeito da educação no estado do Ceará em 2020. Para isso foram elaboradas visualizações que mostram a situação dos alunos e das escolas, da rede pública de ensino e, em grande parte, também das escolas privadas, utilizando dados do Censo Escolar da Educação Básica 2020.

O Censo Escolar é o principal instrumento de coleta de informações da educação básica e a mais importante pesquisa estatística educacional brasileira. É coordenado pelo Instituto Nacional de Estudos e Pesquisas Educacionais Anísio Teixeira (Inep) e realizado em regime de colaboração entre as secretarias estaduais e municipais de educação e com a participação de todas as escolas públicas e privadas do país.

## Tecnologias utilizadas:
  * NodeJs
  * ExpressJs
  * HTML 5
  * CSS 3
  * JavaScript


## Como reproduzir?
Para reproduzir, basta executar os seguintes passos:

0. Clonar o repositório usando o git: 
```
    git clone https://bitbucket.org/DanielAraujoSouza/projetodatavis.git
```

1. Abrir um terminal ou prompt de comando na pasta back-end
2. Instalar as bibliotecas utilizadas com o comando:
```
    npm install
```

3. Executar a aplicação com o comando:
```
    npm run devstart
```

### Equipe:
- Daniel Araújo Chaves Souza - 385186
- Eduardo Bezerra de Lima - 385187
- Neander Danubio Marinho Andrade - 385212

